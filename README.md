# PEC1 
Gerard Pibernat Segarra

En este documento describo la implementación de cada uno de los puntos que se pedía en la práctica. Para jugar hay que utilizar las teclas de dirección.

Enlace al vídeo de demostración: 
[https://youtu.be/gEpPZgW_6uY](https://youtu.be/gEpPZgW_6uY)


## Circuito en terreno montañoso
>*El circuito deberá estar ubicado total o parcialmente en un terreno montañoso, pudiendo también hacer partes urbanas.*

Para construir un circuito de carreras en terreno montañoso he utilizado el sistema Terrain de Unity y la extensión EASyRoads3D, tal como se proponía en las instrucciones guiadas. Toda esta parte la he realizado desde la interfaz gráfica de Unity.

![Circuito](archivos-readme/circuito.png)

## Velocidad reducida fuera de pista
>*Salir fuera de la pista deberá hacer que el coche vaya más lento.*

He implementado esta funcionalidad a través del script **OutOfRoad** asociado al coche del jugador, y he añadido la **etiqueta Road** al **GameObjetc RoadNetwork** y la **etiqueta Terrain** al **GameObject Terrain**.

El script define una ****lista de ruedas (wheelColliders)** y un ****método FixedUpdate** que se llama en cada paso de la física del juego. El método FixedUpdate llama a otro método, **CheckIfWheelsOnRoad**, que comprueba si cada una de las ruedas del coche está en la pista o no.

Para cada rueda del coche, se lanza un rayo hacia abajo desde su posición. Si el rayo impacta con un objeto con la etiqueta "Road", si el rayo impacta con un objeto con la etiqueta "Terrain", se asume que la rueda está fuera de la pista. Dependiendo de dónde se encuentre cada rueda, **se cambia el valor de extremumslip en la curva de fricción**. Este valor controla el máximo deslizamiento de la rueda antes de que la fricción empiece a disminuir y se utiliza para simular el comportamiento de la rueda en diferentes superficies, como asfalto, tierra, nieve, etc.

En resumen, el código comprueba si las ruedas del coche están en la pista o no y ajusta el valor de "extremumslip" para ajustar la velocidad del coche a través de la fricción de las ruedas.

## Carrera de 3 vueltas y coche fantasma
>*La carrera constará de 3 vueltas, y deberéis guardar el fantasma del mejor tiempo (todo el recorrido exacto que haya hecho el vehículo durante esa carrera). El fantasma siempre es el resultado de la mejor vuelta de toda la carrera.*

>*En la siguiente carrera, el jugador deberá correr contra ese fantasma tal como se puede ver en este video.*

Estas funcionalidades están programadas a través de 3 MonoBehaviour, una interfaz y un ScriptableObject:

- **RaceController**: controla los tiempos de la carrera (número de vuelta, tiempo y final), se encarga de actualizar el HUD.
- **GhostLapRecorder**: guarda el recorrido del coche en cada vuelta y mantiene actualizado el ScriptableObject BestLap como la vuelta con mejor tiempo.
- **GhostLapController**: controla al coche fantasma y lo mueve por la pista.
- **LapListener**: interfaz que define el método `void NewLap(float time, int lapNum)`. Esta interfaz es implementada por GhostLapRecorder y GhostLapController. El script RaceController
- **GhostLapData**: clase que almacena los datos de una vuelta.

## Repetición de carrera
>*También se deberá poder ver la repetición de esa carrera, tanto con la cámara normal de juego, como por un conjunto de cámaras que simulen una repetición por televisión al estilo de este video.*

Esta funcionalidad no he conseguido implementarla por completo dentro del plazo de entrega. He empezado la implementación grabando la ruta de igual modo que con el coche fantasma en el **script GhostLapRecorder** y repitiéndola con un duplicado del coche a través del **script WathcReplayController**.

## Tiempos en el HUD
>*Para que el jugador pueda ver sus tiempos deberá mostrarse por pantalla el tiempo de vuelta actual, y los de las dos vueltas anteriores.*

Los tiempos se muestran en la pantalla mediante el script llamado **RaceController**. El script tiene referencias a varios objetos de tipo TextMeshProUGUI que muestra la información sobre el progreso de la carrera. El script también tiene una lista de "escuchadores de nueva vuelta" (LapListener), que son componentes que se suscriben a los eventos de nuevas vueltas para realizar acciones específicas. Este script está asociado al GameObject que forma la línea de meta y detecta los pasos por esta mediante el método OnTriggerEnter().
