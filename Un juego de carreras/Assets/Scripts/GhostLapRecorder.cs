using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostLapRecorder : MonoBehaviour, LapListener
{
    public float timeBetweenSamples = 0.25f;
    public GhostLapData bestLapSO;           // Scriptable object
    public GhostLapData actualLap;			 // Scriptable object
    public GameObject carToRecord;
    private float currenttimeBetweenSamples = 0.0f;

    public GhostLapData allRace;


    // Start is called before the first frame update
    void Start()
    {
        actualLap.Reset();
        allRace.Reset();

    }

    // Update is called once per frame
    void Update()
    {
        // A cada frame incrementamos el tiempo transcurrido 
        currenttimeBetweenSamples += Time.deltaTime;

        // Si el tiempo transcurrido es mayor que el tiempo de muestreo
        if (currenttimeBetweenSamples >= timeBetweenSamples)
        {
            SaveData();
        }


    }

    private void SaveData()
    {
        // Guardamos la información para el fantasma
        actualLap.AddNewData(carToRecord.transform);
        // Guardamos la información para la repetición
        allRace.AddNewData(carToRecord.transform);

        // Dejamos el tiempo extra entre una muestra y otra
        currenttimeBetweenSamples -= timeBetweenSamples;
    }

    public void NewLap(float time, int lapNum)
    {
        if(lapNum >= 1)
        { 
            actualLap.SetLapTime(time);
            bestLapSO.CompareAndReplaceIfTimeMin(actualLap);
            actualLap.Reset();
        }
    }

}
