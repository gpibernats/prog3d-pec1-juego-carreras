using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class UIController : MonoBehaviour, LapListener
{ 
    public Canvas endOfRace;

    // Start is called before the first frame update
    void Start()
    {
        endOfRace.enabled = false;
        GameObject.Find("ReplayCar").SetActive(false);
        GameObject.Find("Car").SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void NewLap(float time, int lapNum)
    {
        if (lapNum >= 3)
        {
            endOfRace.enabled = true;
        }
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void WatchRace()
    {
        Debug.Log("hola");
        GameObject.Find("ReplayCar").SetActive(true);
        GameObject.Find("Car").SetActive(false);
        endOfRace.enabled = false;
    }

}