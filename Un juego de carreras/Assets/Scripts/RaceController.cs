using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class RaceController : MonoBehaviour
{
    public TextMeshProUGUI textoNumVuelta;
    public TextMeshProUGUI textoTiempoVuelta1; // Referencia al componente TextMeshPro de la UI para el tiempo de vuelta 1
    public TextMeshProUGUI textoTiempoVuelta2; // Referencia al componente TextMeshPro de la UI para el tiempo de la vuelta 2
    public TextMeshProUGUI textoTiempoVuelta3; // Referencia al componente TextMeshPro de la UI para el tiempo de la vuelta 3

    public List<LapListener> escuchadoresNuevaVuelta;

    private int numVuelta = 0;
    private float tiempoVuelta1 = 0;
    private float tiempoVuelta2 = 0;
    private float tiempoVuelta3 = 0;
    private float tiempoInicioVueltaActual; // El tiempo de inicio de la vuelta actual
    private float tiempoTranscurridoVueltaActual; // El tiempo transcurrido de la vuelta actual


    void Start()
    {
        textoNumVuelta.text = "0/3";
        textoTiempoVuelta1.text = "";
        textoTiempoVuelta2.text = "";
        textoTiempoVuelta3.text = "";
        escuchadoresNuevaVuelta = new List<LapListener>();
        escuchadoresNuevaVuelta.Add(GetComponent<GhostLapRecorder>());
        escuchadoresNuevaVuelta.Add(GameObject.Find("EventSystem").GetComponent<UIController>());
    }

    void Update()
    {
        tiempoTranscurridoVueltaActual += Time.deltaTime; // Actualiza el tiempo transcurrido de la vuelta actual

        // Actualiza los objetos de texto de la UI con los tiempos de las vueltas
        if (numVuelta == 0) { }
        else if (numVuelta == 1) { textoTiempoVuelta1.text = string.Format("{0:00.00}", tiempoTranscurridoVueltaActual); }
        else if (numVuelta == 2) { textoTiempoVuelta2.text = string.Format("{0:00.00}", tiempoTranscurridoVueltaActual); }
        else if (numVuelta == 3) { textoTiempoVuelta3.text = string.Format("{0:00.00}", tiempoTranscurridoVueltaActual); }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Verifica si el objeto que entra en colisión con la meta es el jugador
        {
            if (numVuelta == 0)
            {
                textoNumVuelta.text = "1/3";
            }
            else if (numVuelta == 1)
            {
                tiempoVuelta1 = tiempoTranscurridoVueltaActual;
                textoTiempoVuelta1.text = string.Format("{0:00.00}", tiempoVuelta1);
                textoNumVuelta.text = "2/3";
            }
            else if (numVuelta == 2)
            {
                tiempoVuelta2 = tiempoTranscurridoVueltaActual;
                textoTiempoVuelta2.text = string.Format("{0:00.00}", tiempoVuelta2);
                textoNumVuelta.text = "3/3";
            }
            else if (numVuelta == 3)
            {
                tiempoVuelta3 = tiempoTranscurridoVueltaActual;
                textoTiempoVuelta3.text = string.Format("{0:00.00}", tiempoVuelta3);
                textoNumVuelta.text = "-";
            }
            SendMessageNewLap();
            tiempoInicioVueltaActual = Time.time; // Inicializa el tiempo de inicio de la vuelta actual
            tiempoTranscurridoVueltaActual = 0f; // Reinicia el tiempo transcurrido de la vuelta actual
            numVuelta++;
        }
    }

    void SendMessageNewLap()
    {
        foreach (LapListener listener in escuchadoresNuevaVuelta)
        {
            listener.NewLap(tiempoTranscurridoVueltaActual, numVuelta);
        }
    }


}
