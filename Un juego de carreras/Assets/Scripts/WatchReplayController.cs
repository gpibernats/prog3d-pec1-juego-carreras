using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchReplayController : MonoBehaviour
{

    public float timeBetweenSamples = 0.25f;
    public GhostLapData allRace;           // Scriptable object    
    public GameObject carToPlay;
    private float currenttimeBetweenSamples = 0.0f;

    private int currentSampleToPlay = 0;
    private Vector3 lastSamplePosition, nextPosition;
    private Quaternion lastSampleRotation, nextRotation;


    // Start is called before the first frame update
    void Start()
    {

        //bestLapSO.Reset();
        currentSampleToPlay = 0;
        lastSamplePosition = carToPlay.transform.position;
        lastSampleRotation = carToPlay.transform.rotation;


    }

    // Update is called once per frame
    void Update()
    {
        // A cada frame incrementamos el tiempo transcurrido 
        currenttimeBetweenSamples += Time.deltaTime;


        // Si el tiempo transcurrido es mayor que el tiempo de muestreo
        if (currenttimeBetweenSamples >= timeBetweenSamples)
        {
            if (allRace.Count() > 0)
            {
                ReadAndMove();
            }
            
        }


    }

    private void ReadAndMove()
    {
        // De cara a interpolar de una manera fluida la posición del coche entre una muestra y otra,
        // guardamos la posición y la rotación de la anterior muestra
        lastSamplePosition = nextPosition;
        lastSampleRotation = nextRotation;

        // Cogemos los datos del scriptable object
        allRace.GetDataAt(currentSampleToPlay, out nextPosition, out nextRotation);

        // Dejamos el tiempo extra entre una muestra y otra
        currenttimeBetweenSamples -= timeBetweenSamples;

        // Incrementamos el contador de muestras
        currentSampleToPlay++;
        if (currentSampleToPlay >= allRace.Count())
        {
            currentSampleToPlay = 0;
        }

        float percentageBetweenFrames = currenttimeBetweenSamples / timeBetweenSamples;

        carToPlay.transform.position = Vector3.Slerp(lastSamplePosition, nextPosition, percentageBetweenFrames);
        carToPlay.transform.rotation = Quaternion.Slerp(lastSampleRotation, nextRotation, percentageBetweenFrames);
    }

}
