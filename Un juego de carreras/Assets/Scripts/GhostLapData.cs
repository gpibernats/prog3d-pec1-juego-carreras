using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GhostLapData : ScriptableObject
{
    List<Vector3> carPositions;
    List<Quaternion> carRotations;
    private float lapTime = float.PositiveInfinity;

    public void AddNewData(Transform transform)
    {
        carPositions.Add(transform.position);
        carRotations.Add(transform.rotation);
    }

    public void GetDataAt(int sample, out Vector3 position, out Quaternion rotation)
    {
        //Debug.Log(sample + " " + carPositions.Count);
        position = carPositions[sample];
        rotation = carRotations[sample];
    }

    public void SetLapTime(float time)
    {
        lapTime = time;
    }

    public void Reset()
    {
        //carPositions.Clear();
        //carRotations.Clear();
        carPositions = new List<Vector3>();
        carRotations = new List<Quaternion>();
        lapTime = float.PositiveInfinity;
    }

    public void CompareAndReplaceIfTimeMin(GhostLapData other)
    {
        if (other.lapTime < lapTime)
        {
            carPositions = other.carPositions;
            carRotations = other.carRotations;
            lapTime = other.lapTime;
        }
    }

    public void Copy(GhostLapData other)
    {
        carPositions = other.carPositions;
        carRotations = other.carRotations;
        lapTime = other.lapTime;
    
    }

    public int Count()
    {
        return carRotations.Count;
    }

 
}