using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class OutOfRoad : MonoBehaviour
{

    public List<WheelCollider> wheelColliders;



    void FixedUpdate()
    {
        // Comprobar si las ruedas del coche están en la pista.
        CheckIfWheelsOnRoad();
    }

    void CheckIfWheelsOnRoad()
    {
        // Comprobar cada rueda del coche.
        foreach (WheelCollider wheel in wheelColliders)
        {
            RaycastHit hit;

            // Lanzar un rayo desde la posición de la rueda hacia abajo.
            if (Physics.Raycast(wheel.transform.position, -wheel.transform.up, out hit, wheel.radius + 0.18f))
            {
                // Comprobar si el objeto impactado es la pista.
                if (hit.collider.CompareTag("Road"))
                {

                    // La rueda está en la pista.
                    //Debug.Log("Dentro");


                    // Obtener la curva de fricción hacia adelante actual de la rueda.
                    WheelFrictionCurve forwardFrictionCurve = wheel.forwardFriction;
                    // Cambiar el valor de extremum slip de la curva de fricción hacia adelante.
                    forwardFrictionCurve.extremumSlip = 0.4f;
                    // Asignar la nueva curva de fricción hacia adelante a la rueda.
                    wheel.forwardFriction = forwardFrictionCurve;

                }
                else if (hit.collider.CompareTag("Terrain"))
                {
                  
                    // La rueda está fuera de la pista.
                    //Debug.Log("Fuera");


                    // Obtener la curva de fricción hacia adelante actual de la rueda.
                    WheelFrictionCurve forwardFrictionCurve = wheel.forwardFriction;
                    // Cambiar el valor de extremum slip de la curva de fricción hacia adelante.
                    forwardFrictionCurve.extremumSlip = 5f;
                    // Asignar la nueva curva de fricción hacia adelante a la rueda.
                    wheel.forwardFriction = forwardFrictionCurve;

                }
            }
        }
    }






}
