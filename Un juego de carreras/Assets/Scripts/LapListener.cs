using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface LapListener
{

    void NewLap(float time, int lapNum);

}
