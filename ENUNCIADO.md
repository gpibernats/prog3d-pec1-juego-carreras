# Programación en Unity 3D
## PEC 1: Carreras Contrarreloj

La asignatura Programación en Unity 3D se evalúa con tres Prácticas de Evaluación Continua (PEC) y una práctica final.

### Fechas importantes de la PEC1

Fecha de publicación del enunciado: 06/03/2023.

Fecha de entrega: 02/04/2023.

Fecha de calificación: 16/04/2023.

### Cálculo de la nota final

La nota final se calculará en base a las notas individuales de cada PEC. Al ser una evaluación continua, la evolución del alumno durante la asignatura y su participación en los debates y actividades propuestas en el aula también se tendrá en cuenta a la hora de fijar el resultado final.

### Librerías necesarias para hacer las prácticas

Esta práctica la vamos a realizar usando el motor de videojuegos Unity 3D en su versión **2021.3.19f1 LTS**.

### Sistemas operativos aceptados

Os recomendamos que realicéis las prácticas en un entorno Windows o Mac OS. Si necesitáis licencias de Windows, Microsoft Visual Studio o cualquier otro software, comentadlo en el foro de la asignatura y os indicaremos si desde la UOC os las podemos proporcionar.

### ¿Qué se tiene que entregar?

Se deberá subir el proyecto funcionando sin errores al GitLab, y que este contenga **TODOS** los siguientes elementos:

- Un **TAG identificativo (PEC1)** en la versión final que queréis que sea evaluada.
- Un fichero **README.md** que explique cómo ejecutar el proyecto si es que hay algún requisito extra, así como unas instrucciones que expliquen los controles del juego o si hay que hacer alguna acción específica en concreto para poder jugarlo.
- En ese mismo Readme, se deberá incluir una descripción explicando qué partes habéis implementado y cómo lo habéis hecho.
- Un link a un vídeo (de una duración aproximada de 2 minutos) que enseñe el funcionamiento de todos los puntos que se han implementado donde se os escuche a vosotros explicando las funcionalidades creadas.

### Descripción del juego propuesto

Siguiendo el hilo del documento “Un juego de carreras”, os proponemos que implementéis un juego de carreras en el que lo importante sea un modo **Time Trial (Modo Contrarreloj)**. En este modo el jugador debe correr para superar su propio récord.

Los puntos básicos que debéis implementar son:

1.	El circuito deberá estar ubicado total o parcialmente en un terreno montañoso, pudiendo también hacer partes urbanas. 
2.	Salir fuera de la pista deberá hacer que el coche vaya más lento.
3.	La carrera constará de 3 vueltas, y deberéis guardar el fantasma del mejor tiempo (todo el recorrido exacto que haya hecho el vehículo durante esa carrera). El fantasma siempre es el resultado de la mejor vuelta de toda la carrera.
4.	En la siguiente carrera, el jugador deberá correr contra ese fantasma tal como se puede ver [en este video](https://www.youtube.com/watch?v=AvHVmOUa5So).
5.	También se deberá poder ver la repetición de esa carrera, tanto con la cámara normal de juego, como por un conjunto de cámaras que simulen una repetición por televisión al estilo de [este video](https://www.youtube.com/watch?v=W4o-jd8iEqo). 
<!---
The original video is from 2009, quite old. I've replaced it by a game from 2019. 
[este video](https://www.youtube.com/watch?v=gtApCep7Syk).
-->
6.	Para que el jugador pueda ver sus tiempos deberá mostrarse por pantalla el tiempo de vuelta actual, y los de las dos vueltas anteriores.

Los puntos que valoraremos son:
1.	Definir una buena estructura de datos para guardar la información del juego.
2.	Claridad y sencillez en el código.
3.	Buena documentación y explicación del trabajo realizado.

Los puntos optativos que podéis implementar una vez implementados los puntos anteriores son:
1.	Crear diferentes circuitos.
2.	Tener diferentes coches con configuraciones distintas.
3.	Tener un menú con el que seleccionar entre los diferentes circuitos y coches.
4.	Usar JSON o XML para guardar la información en el fichero.
5.	Añadir sonido y FX.
6.	Leer los datos del fantasma de un fichero de texto usando Resources de Unity.
7.	Añadir daño a los coches y que empeoren si chocamos.
8.	Colocar diferentes atajos por los circuitos.

Como punto optativo extra, está la posibilidad de implementar las camaras de la repetición de la carrera (punto obligatorio num.5) utilizando el sistema '[Cinemachine](https://unity.com/es/unity/features/editor/art-and-design/cinemachine)' de Unity. 

Para implementar este último punto, os recomendamos las siguientes fuentes de información oficiales donde encontraréis todo lo necesiario para empezar a trabajar con Cinemachine:

- [Unity Documentation - Cinemachine Manual](https://docs.unity3d.com/Packages/com.unity.cinemachine@2.9/manual/index.html)

- [Unity Documentation - Cinemachine Script Reference](https://docs.unity3d.com/Packages/com.unity.cinemachine@2.9/api/index.html)
<!---
This link is not valid anymore
- [Unity Learn - Using Tutorials](https://learn.unity.com/tutorial/cinemachine?uv=2017.1)
-->
- [EXTRA: Unity Github - Cinemachine Package](https://github.com/Unity-Technologies/com.unity.cinemachine)

